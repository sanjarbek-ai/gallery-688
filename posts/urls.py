from django.urls import path

from posts.views import home_page_view, add_post_view, post_detail_view, comment_view

app_name = 'posts'

urlpatterns = [
    path('add/post/', add_post_view, name='add'),
    path('post/<int:pk>/', post_detail_view, name='detail'),
    path('post/comment/<int:pk>/', comment_view, name='comment'),
    path('', home_page_view, name='home'),
]

