from django.contrib import admin

from posts.models import PostModel, CommentModel

admin.site.register(PostModel)
admin.site.register(CommentModel)
