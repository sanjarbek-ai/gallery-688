from django.contrib.auth.models import AbstractUser
from django.db import models


class UserModel(AbstractUser):
    avatar = models.ImageField(upload_to='avatars/', blank=True, null=True)
    bio = models.TextField(null=True, blank=True)

    def __str__(self):
        return self.get_full_name()

    class Meta:
        verbose_name = 'user'
        verbose_name_plural = 'users'


class VerificationCodeModel(models.Model):
    email = models.EmailField()
    code = models.CharField(max_length=4, unique=True)
    status = models.BooleanField(default=False)
    send_time = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.code

    class Meta:
        verbose_name = 'Code'
        verbose_name_plural = 'Codes'


class FollowersModel(models.Model):
    which_user = models.ForeignKey(UserModel, on_delete=models.CASCADE, related_name='which')
    whom_user = models.ForeignKey(UserModel, on_delete=models.CASCADE, related_name='whom')

    class Meta:
        verbose_name = 'Follower'
        verbose_name_plural = 'Followers'
