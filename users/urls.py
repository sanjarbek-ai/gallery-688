from django.urls import path

from users.views import register_view, login_view, logout_view, user_profile, verify_code_view, update_user_view, \
    following_view, get_user_by_username

app_name = 'users'

urlpatterns = [
    path('register/', register_view, name='register'),
    path('login/', login_view, name='login'),
    path('logout/', logout_view, name='logout'),
    path('profile/', user_profile, name='profile'),
    path('verify/', verify_code_view, name='verify'),
    path('update/', update_user_view, name='update'),
    path('follow/<int:pk>/w', following_view, name='follow'),
    path('username/<int:pk>/', get_user_by_username, name='username'),
]